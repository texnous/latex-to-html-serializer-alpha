#!/usr/bin/env node
const fs = require("fs");
// const beautify = require('js-beautify').html;
const path = require("path");

const LatexStyle = require("@texnous/latex-syntax"); // LaTeX style structures
const LatexParser =  require("@texnous/latex-parser"); // LaTeX parser class

String.prototype.capitalize = function capitalize() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};
const type = process.argv[2] || "json"; // json | html

const Converter =  require(`./lib/AstTo${type.capitalize()}`); // LaTeX parser class
let latexStyle = new LatexStyle();
latexStyle.loadPackage("test", require("./latex-style.json"));
let latexParser = new LatexParser(latexStyle);
let converter = new Converter();

let latexDir = path.join(__dirname, "latex");
let outputDir = path.join(__dirname, type);
let cssFilePath = path.join(__dirname, "base.css");
let css = fs.readFileSync(cssFilePath, { encoding: "utf-8" });

fs.readdirSync(latexDir).forEach(function(file) {
	// if (file !== "t.tex") return;
	console.log(`proccess file '${file}' to ${type}`);

	let latexFilePath = path.join(latexDir, path.basename(file));

	let contents = fs.readFileSync(latexFilePath, { encoding: "utf-8" });
	let context = new LatexParser.Context(contents);
	context.offset = context.source.indexOf("\\begin{document}");
	if (context.offset < 0) return;
	let latexRootToken = latexParser.parse(contents.substring(context.offset), context)[0];

	let result = converter.convert(latexRootToken);
	let jsonFilePath = path.join(outputDir, `${path.basename(file, ".tex")}.${type}`);

	if (type === "json"){
		result = JSON.stringify(result);
	} else {
		result = `<style>
			${css}
		</style>
		${result}`;
		//result = beautify(result)
	}

	fs.writeFileSync(jsonFilePath, result, { encoding: "utf-8" });
});




