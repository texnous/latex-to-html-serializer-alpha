const LatexTree = require("@texnous/latex-tree"); // LaTeX tree structure elements

module.exports = class Converter {
	convert(token, context) {
		const tokenNames = [
			"SourceToken",
			"SpaceToken",
			"EnvironmentBodyToken",
			"EnvironmentToken",
			"CommandToken",
			"SymbolToken",
			"ParameterToken",
			"Token"
		];

		const name = tokenNames.find(name => token instanceof LatexTree[name]);

		if(name) {
			return this[`_convert${name}`](token, context);
		}

		throw new TypeError("\"token\" isn\'t an LatexTree.Token instance");
	}

	_convertToken() {
		throw Error("_convertToken not implemented in child class");
	}

	_convertParameterToken() {
		throw Error("_convertParametrToken not implemented in child class");
	}

	_convertSymbolToken() {
		throw Error("_convertSymbolToken not implemented in child class");
	}

	_convertCommandToken() {
		throw Error("_convertCommandToken not implemented in child class");
	}

	_convertEnvironmentToken() {
		throw Error("_convertEnvironmentToken not implemented in child class");
	}

	_convertEnvironmentBodyToken() {
		throw Error("_convertEnvironmentBodyToken not implemented in child class");
	}
	_convertSpaceToken() {
		throw Error("_convertSpaceToken not implemented in child class");
	}

	_convertSourceToken() {
		throw Error("_convertSourceToken not implemented in child class");
	}
};
