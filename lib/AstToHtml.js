const Converter = require("./Converter");
const fPath = require("path");
const fs = require("fs");
const { pathOr, path } = require("ramda");

const fullInfo = (token, result, classes) => {
	// return `<span>${result}</span>`;
	let classPart = "";

	if(classes) {
		classPart =`class="${typeof classes === "string" ? classes : classes.join(" ")}"`;
	}

	return `<span
		${classPart}
		data-token-name="${token.name}"
		data-token-lexeme="${token.lexeme}"
		data-token-class="${token.constructor.name}"
 	>${result}</span>`;
};

const printError = e => `<div style="border: 1px solid red; padding: 10px;">${e.toString()}</div>`;

const standartMathEnvs = [
	"align",
	"aligned",
	"equation",
];

class HtmlContext {
	constructor() {
		this.stateStack = [];
		this.metaInfo = {
			bibItems: []
		};
		this.metaTags = [];
	}

	updateModes(operations) {
		if (!operations.length) {
			return;
		}

		const nextState = {};
		const pureOperations = operations.filter(o => o.operand !== "GROUP");
		const endDirective = operations.filter(o => o.operand === "GROUP")[0];
		if (endDirective) {
			if (endDirective.directive === "END") {
				this.stateStack.reverse();
				const groupIndex = this.stateStack.findIndex(s => s.GROUP);

				this.stateStack.splice(groupIndex, 1);

				this.stateStack.reverse();
			} else {
				nextState.GROUP = true;
			}
		}

		const beginDerectives = pureOperations.filter(o => o.directive === "BEGIN").map(o => o.operand);
		const endDerectives = pureOperations.filter(o => o.directive === "END").map(o => o.operand);

		beginDerectives.forEach(operand => {
			nextState[operand] = true;
		});

		endDerectives.forEach(operand => {
			nextState[operand] = false;
		});

		if (Object.keys(nextState).length) {
			this.stateStack.push(nextState);
		}

	}

	isModeEnabled(name) {
		return this.stateStack.find(state => state[name]);
	}
}


class AstToHtml extends Converter {
	convert(token, context) {
		let result;

		if (!context) {
			context = this.getContext();
			result = super.convert(token, context);

			if (context.metaInfo.bibItems) {
				context.metaInfo.bibItems.forEach((name, index) => {
					result = result.replace(RegExp(`\\\\cite{${name}}`, "g"), `[<a href="#${name}">${index + 1}</a>]`);
				});
			}


			result = `
				<html>
					<head>
						<meta charset="utf-8" />
						${context.metaTags.join("\n")}
						<script type="text/x-mathjax-config">
						MathJax.Hub.Config({
							tex2jax: {
								inlineMath: [['$','$'], ['$$','$$'], ['\\\\[','\\\\]']],
								processEscapes: true,
								processClass: "math"
							}
						});
						</script>
						<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
					</head>
					<body>
					${result}
					</body>
				</html>
				`;
		} else {
			result = super.convert(token, context);
		}

		return result;
	}

	getContext() {
		return new HtmlContext()
	}

	_convertToken(token, context, h) {
		const children = token.childNodes.map(token => this.convert(token, context));

		return children.join("");
	}

	_convertParameterToken(token, context) {
		if (context.isModeEnabled("MATH")) {
			return token.childNodes.map(c => c.toString(false)).join('');
		}

		return this._convertToken(token, context);
	}

	_convertSymbolToken(token, context) {
		const style = (token.symbol || token.command);
		const preOperationsGroup = style && style.parameters.find(p => p.operations);

		if (preOperationsGroup){
			context.updateModes(preOperationsGroup.operations);
		}

		const children = token.childNodes.map(token => this.convert(token, context));
		let result = pathOr(token.pattern || "", ["html", "pattern"], style);
		let meta = path(["html", "meta"], style);

		if (meta) {
			const childrenPure = token.childNodes.map(c => c.childNodes.map(token => this.convert(token, context)).join(""));

			for(let i = 0; i < childrenPure.length; i++) {
				result = result.replace(RegExp((`#${i+1}`), "g"), childrenPure[i]);
			}

			for(let i = 0; i < childrenPure.length; i++) {
				meta = meta.replace(RegExp((`#${i+1}`), "g"), childrenPure[i]);
			}
			context.metaInfo[token.name] = result;
			context.metaTags.push(meta);
			return "";
		}

		for(let i = 0; i < children.length; i++) {
			result = result.replace(`#${i+1}`, children[i]);
		}

		style && context.updateModes(style.operations);

		return result;
	}

	_convertCommandToken(token, context) {
		let children;

		switch(token.name) {
		case "bibitem":
			const citeSource = token.childNodes[0].childNodes[0].source;

			context.metaInfo.bibItems.push(citeSource);
			children = token.childNodes[0].childNodes.map(token => this.convert(token, context));
			return `\n<li id="${citeSource}">\n`;
		case "thanks":
		case "titleEng":
		case "authorEng":
		case "abstractEng":
		case "organizationEng":

			children = token.childNodes[0].childNodes.map(token => this.convert(token, context));
			context.metaInfo[token.name] = children.join("");
			return "";
		case "includegraphics":
			try {
				const src = token.childNodes[1].childNodes[0].source.replace(".eps", ".svg");
				const filePath = fPath.join(fPath.join(__dirname, "..", "html", "img"), src);
				return fs.readFileSync(filePath, {encoding: "utf-8"});
			} catch (e) {
				return printError(e);
			}
		case "cite":
			return token.toString(false);
		case "maketitle":
			return `<div class="titlepage">
					${context.metaInfo.title || ""}
					${context.metaInfo.organization || ""}
					${context.metaInfo.author || ""}
					${context.metaInfo.email || ""}
					${context.metaInfo.abstract || ""}
					<h3>${context.metaInfo.titleEng || ""}</h3>
					<h4>${context.metaInfo.organizationEng || ""}</h4>
					<h5>${context.metaInfo.authorEng || ""}</h5>
					<p class='abstract'>${context.metaInfo.abstractEng || ""}</p>
				</div>`;
		}

		const result = this._convertSymbolToken(token, context);

		if (context.isModeEnabled("MATH")) {
			return `\\${token.name} ${result}`;
		}

		return result || "";
	}

	_convertEnvironmentToken(token, context) {
		if (standartMathEnvs.includes(token.name)){
			return fullInfo(token, token.toString(false), "math");
		}

		return this._convertToken(token, context)
	}

	_convertEnvironmentBodyToken(token, context) {
		return this._convertToken(token, context, true);
	}

	_convertSpaceToken(token, context) {
		return token.lineBreakCount > 1 ? "\n<br/>\n" : " ";
	}

	_convertSourceToken(token, context) {
		return this._convertToken(token, context);
	}
}

module.exports = AstToHtml;