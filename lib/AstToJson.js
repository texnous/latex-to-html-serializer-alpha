const Converter = require("./Converter");

module.exports = class AstToJson extends Converter {
	_convertToken(token, context) {
		const json = {};
		json.tokenType = "Token";

		if (token.childNodes.length > 0) {
			json.children = token.childNodes.map(token => this.convert(token, context));
		}
		return json;
	}

	_convertParameterToken(token, context) {
		const json = this._convertToken(token, context);
		json.tokenType = "ParameterToken";
		json.hasBrackets = token.hasBrackets;
		json.hasSpacePrefix = token.hasSpacePrefix;
		return json;
	}

	_convertSymbolToken(token, context) {
		const json = {};
		json.tokenType = "SymbolToken";

		if (token.pattern) {
			json.pattern = token.pattern;
		}

		if (token.symbol) {
			if (token.childNodes.length > 0) {
				json.children = token.childNodes.map((childToken, childIndex) => {
					return this.convert(childToken, context);
				});
			}
		}
		return json;
	}

	_convertCommandToken(token, context) {
		const json = this._convertSymbolToken(token, context);
		json.tokenType = "CommandToken";
		json.name = token.name;
		return json;
	}

	_convertEnvironmentToken(token, context) {
		const json = this._convertToken(token, context);
		json.tokenType = "EnvironmentToken";
		json.name = token.name;
		return json;
	}

	_convertEnvironmentBodyToken(token, context) {
		const json = this._convertToken(token, context);
		json.tokenType = "EnvironmentBodyToken";
		return json;
	}
	_convertSpaceToken(token, context) {
		const json = this._convertToken(token, context);
		json.tokenType = "SpaceToken";
		return json;
	}

	_convertSourceToken(token, context) {
		const json = this._convertToken(token, context);
		json.tokenType = "SourceToken";
		json.source = token.source;
		return json;
	}
};