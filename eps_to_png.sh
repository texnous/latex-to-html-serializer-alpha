#!/usr/bin/env bash

cd ./html/img/
for f in *.eps
do
  echo "Processing $f file..."
  inkscape $f -l "`basename "$f" .eps`.svg"
done